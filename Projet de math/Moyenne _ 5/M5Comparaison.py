#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Oct 27 15:07:38 2019
@title : Comparaison des systemes de recommandation
@author : LY Thomas - SAINT-SAENS Hugo
@version : 1.0
"""

import csv
import numpy as np

# Constantes caractérisant la taille du jeu de données proposé
NB_UTILISATEUR = 100
NB_ITEM = 1000

# Retourne les valeurs du fichier .csv dans une matrice
def lecture_csv(fichier):
    donnees = np.zeros((NB_UTILISATEUR,NB_ITEM))
    utilisateurCourant = 0
    with open(fichier, 'r') as csvfile:
        filereader = csv.reader(csvfile, delimiter=' ')
        for row in filereader:
            for j in range(NB_ITEM):
                donnees[utilisateurCourant,j] = row[j]
            utilisateurCourant+=1
    return donnees;

# Retourne le pourcentage de note correctement prédite en fonction du nombre de notes non note
def getPourcentageCorrect(matrice):
    itemCorrectementPredit = 0
    itemNonNote = 0
    for x in range(len(donneesVide)):
        for y in range(len(donneesVide[x])):
            if donneesVide[x][y] == -1:
                itemNonNote += 1
                if donneesPleine[x][y] == matrice[x][y]:
                    itemCorrectementPredit += 1
    return (itemCorrectementPredit / itemNonNote) * 100

donneesVide = lecture_csv('./toy_incomplet.csv').astype(np.int)
donneesPleine = lecture_csv('./toy_complet.csv').astype(np.int)
itemBased = lecture_csv('./collaborative_filtering_item_based.csv').astype(np.int)
userBased = lecture_csv('./collaborative_filtering_user_based.csv').astype(np.int)

percentItemBased = getPourcentageCorrect(itemBased)
percentUserBased = getPourcentageCorrect(userBased)

print("Item-based : ",round(percentItemBased,2),"%")
print("User-based : ",round(percentUserBased,2),"%")