#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 18 08:26:13 2019
@title : Collaborative filtering - User-Based
@author : LY Thomas - SAINT-SAENS Hugo
@version : 1.0
"""

import csv
import numpy as np
import math as math

# Constantes caractérisant la taille du jeu de données proposé
NB_UTILISATEUR = 100
NB_ITEM = 1000
# Constante caractérisant le nombre d'utilisateurs que l'on prend en compte pour calculer la moyenne
NB_TOP = 5

# Retourne les valeurs du fichier .csv dans une matrice
def lecture_csv():
    donnees = np.zeros((NB_UTILISATEUR,NB_ITEM))
    utilisateurCourant = 0
    with open('./toy_incomplet.csv', 'r') as csvfile:
        filereader = csv.reader(csvfile, delimiter=' ')
        for row in filereader:
            for j in range(NB_ITEM):
                donnees[utilisateurCourant,j] = row[j]
            utilisateurCourant+=1
    return donnees;

# Permet d'écrire dans un nouveau fichier .csv la matrice avec les items predit
def ecriture_csv(matrice):
    with open('collaborative_filtering_user_based.csv','w') as csvfile:
        filewriter = csv.writer(csvfile)
        for u in range(len(matrice)):
            chaine = ''
            for i in range(len(matrice[u])):
                chaine += str(matrice[u][i])+' '
            filewriter.writerow([chaine])

# Retourne la moyenne d'un utilisateur
def moyenne(k,matrice):
    somme = 0.0
    Nbitems = 0
    for j in range(len(matrice[k])):
        if matrice[k][j] != -1:
            somme = matrice[k,j] + somme
            Nbitems += 1
    if Nbitems == 0:
        return 0
    else:
        return somme / Nbitems

# Retourne la covariance entre l'utilisateur x et y à l'aide de leurs moyennes
def cov(x,y,m1,m2):
    cov = 0
    for j in range(len(matrixItemCommun[y])):
        if (matrixItemCommun[x,j] != -1 and matrixItemCommun[y,j] != -1):
            cov = cov + ((matrixItemCommun[x,j] - m1) * (matrixItemCommun[y,j] - m2))
    return (cov / len(matrixItemCommun[y]))

# Retourne la variance d'un utilisateur à l'aide de sa moyenne
def var(k,m,matrice):
    v = 0
    for j in range(len(matrice[k])):
        if (matrice[k,j] != -1):
            v = v + (matrice[k,j] - m)**2
    return (v / len(matrice[k]))

# Retourne la similarité entre l'utilisateur x et y à l'aide de leur covariance commune et de leurs variances
def similarite(x,y,c,varx,vary):
    deno = math.sqrt(varx) * math.sqrt(vary)
    for j in range(len(matrixItemCommun[y])):
        if (matrixItemCommun[x,j] != -1 and matrixItemCommun[y,j] != -1):
            if deno != 0:
                return (c / deno)
            else:
                return 0

# Classe caractérisant un utilisateur à l'aide de sa place dans la matrice ainsi que sa similarité 
class Utilisateur:
    def __init__(self,numero):
        self.numero=numero
        self.similarite=0.0
    
    def setSimilarite(self,s):
        self.similarite=s
    
    def getNumero(self):
        return self.numero

# Retourne la moyenne des notes des 5 premiers
def moyenneItems(top5liste,itemCourant,matrice):
    somme = 0
    nbElement = 0
    for i in range(len(top5liste)):
        if matrice[top5liste[i].getNumero()][itemCourant] != -1:
            somme += matrice[top5liste[i].getNumero()][itemCourant]
            nbElement += 1
    if nbElement == 0:
        return 0
    else:
        return round(somme / nbElement)

# Nous créons la matrice correspondant au fichier .csv donné
donnees = lecture_csv().astype(np.int)
# Nous dupliquons la matrice précédente pour obtenir une matrice immuable 
# afin d'analyser quels sont les items en commun d'un utilisateur et d'un autre
donneesDeBase = lecture_csv().astype(np.int)

# Nous parcourons les utilisateurs
for utilisateurCourant in range(len(donneesDeBase)):
    # Nous calculons la moyenne et la variance de l'utilisateur courant
    moyenneUtilisateurCourant = moyenne(utilisateurCourant,donneesDeBase)
    varx = var(utilisateurCourant,moyenneUtilisateurCourant,donneesDeBase)
    # Nous parcourons les items de l'utilisateur actuel
    for itemCourant in range(len(donneesDeBase[utilisateurCourant])):
        print("utilisateur n°",utilisateurCourant," item n°",itemCourant)
        # Si un item est trouvé sans note alors
        if donneesDeBase[utilisateurCourant][itemCourant] == -1:
            # Nous créons une nouvelle matrice initialisée avec des zéros 
            # et qui a pour taille le jeu de données voulu
            matrixItemCommun=np.zeros(shape=(NB_UTILISATEUR,NB_ITEM)).astype(np.int)
            # Nous y insèrons les items de l'utilisateur actuel 
            matrixItemCommun[utilisateurCourant]=donneesDeBase[utilisateurCourant]
            # Nous créons une liste vide d'utilisateurs
            listeUtilisateur = []
            # Nous parcourons de nouveau la matrice pour connaitre les items en commun de l'utilisateur courant avec l'utilisateur comparé
            for utilisateurCompare in range(len(donneesDeBase)):
                # Nous créons l'utilisateur compare à l'aide de sa place dans la matrice 
                utilisateur=Utilisateur(utilisateurCompare)
                # Nous parcourons les items de celui-ci
                for itemCompare in range(len(donneesDeBase[utilisateurCompare])):
                    # Si l'utilisateur courant est différent de l'utilisateur comparé alors 
                    if (utilisateurCourant != utilisateurCompare): 
                        # Si les deux utilisateurs ont un item en commun alors
                        if (donneesDeBase[utilisateurCompare][itemCompare] != -1) and (donneesDeBase[utilisateurCourant][itemCompare] != -1):
                            # Nous insèrons dans la matrice la note de l'item compare de l'utilisateur comparé
                            matrixItemCommun[utilisateurCompare][itemCompare]=donneesDeBase[utilisateurCompare][itemCompare]
                        else:
                            # Sinon nous y insèrons un item non noté
                            matrixItemCommun[utilisateurCompare][itemCompare]=-1
                # Nous ajoutons l'utilisateur dans la liste d'utilisateurs
                listeUtilisateur += [utilisateur]
                # Si l'utilisateur courant est différent de l'utilisateur comparé alors
                if (utilisateurCourant != utilisateurCompare):
                    # Nous calculons la moyenne de l'utilisateur comparé ainsi que sa similarité 
                    # que l'on ajoute à l'utilisateur en question qui se trouve dans la liste d'utilisateurs 
                    moyenneUtilisateurCompare = moyenne(utilisateurCompare,matrixItemCommun)
                    listeUtilisateur[utilisateurCompare].setSimilarite(similarite(utilisateurCourant,utilisateurCompare,cov(utilisateurCourant,utilisateurCompare,moyenneUtilisateurCourant,moyenneUtilisateurCompare),varx,var(utilisateurCompare,moyenneUtilisateurCompare,matrixItemCommun)))
            # Nous créons une liste comportant les NB_TOP premiers utilisateurs ayant la plus grande similarité
            top=sorted(listeUtilisateur, key=lambda u: u.similarite, reverse=True)[:NB_TOP]
            # Nous vidons la liste d'utilisateurs pour une utilisation ultérieur
            del listeUtilisateur[:]
            # Nous insérons la moyenne des items des NB_TOP premiers utilisateurs dans la première matrice 
            donnees[utilisateurCourant][itemCourant] = moyenneItems(top,itemCourant,donnees)
            # Nous vidons la liste des NB_TOP premiers utilisateurs
            # ayant la plus grande similarité pour une utilisation ultérieur
            del top[:]
# Nous créons un nouveau fichier .csv avec les notes prédites
ecriture_csv(donnees)