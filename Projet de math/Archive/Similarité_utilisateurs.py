#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 18 08:26:13 2019
@author: LY Thomas - SAINT-SAENS Hugo
"""

import csv
import numpy as np
import math as math

# constante
NB_UTILISATEUR = 5
NB_ITEM = 10

# permet d'insérer les  valeurs du fichier .csv dans une matrice
def lecture_csv():
    donnees = np.zeros((NB_UTILISATEUR,NB_ITEM)) #initialise la matrice [Utilisateurs,Items] à zéros
    utilisateurCourant = 0
    with open('./5par10.csv', 'r') as csvfile:
        filereader = csv.reader(csvfile, delimiter=' ')
        for row in filereader:
            for j in range(NB_ITEM): #rempli la matrice avec les données du fichier csv
                donnees[utilisateurCourant,j] = row[j]
            utilisateurCourant+=1 #permet de passer à l'utilisateur suivant
    return donnees;

# calcul de chaque moyennes des utilisateurs
def moyenne(k):
    somme = 0.0
    Nbitems = 0
    for j in range(len(matrixItemCommun[k])):
        if matrixItemCommun[k][j] != -1:
            somme = matrixItemCommun[k,j] + somme
            Nbitems += 1
    return (somme / Nbitems)

# calcul de la covariance entre l'utilisateur x et y
def cov(x,y,m1,m2):
    cov = 0
    for j in range(len(matrixItemCommun[utilisateurCompare])):
        if (matrixItemCommun[x,j] != -1 and matrixItemCommun[y,j] != -1):
            cov = cov + ((matrixItemCommun[x,j] - m1)*(matrixItemCommun[y,j] - m2))
    return (cov / len(matrixItemCommun[utilisateurCompare]))

# calcul de la variance entre l'utilisateur x et y
def var(k,m):
    v = 0
    for j in range(len(matrixItemCommun[utilisateurCompare])):
        if (matrixItemCommun[k,j] != -1):
            v = v + (matrixItemCommun[k,j]-m)**2
    return (v / len(matrixItemCommun[utilisateurCompare]))

# calcul de la similarité entre l'utilisateur x et y
def similarite(x,y,c,varx,vary):
    deno = math.sqrt(varx)*math.sqrt(vary)
    for j in range(len(matrixItemCommun[utilisateurCompare])):
        if (matrixItemCommun[x,j] != -1 and matrixItemCommun[y,j] != -1):
            if deno != 0:
                return (c / deno)
            else:
                return 0

class Utilisateur:
    def __init__(self,numero):
        self.numero=numero
        self.similarite=0.0
    
    def setSimilarite(self,s):
        self.similarite=s
    
    def getNumero(self):
        return self.numero

def moyenneItems(top5liste,itemCourant,matrice):
    somme = 0
    nbElement = 0
    for i in range(len(top5liste)):
        if matrice[top5liste[i].getNumero()][itemCourant] != -1:
            somme += matrice[top5liste[i].getNumero()][itemCourant]
            nbElement += 1
    if nbElement == 0:
        return 0
    else:
        return somme / nbElement

# On creer une premiere matrice pour pourvoir y inserer les notes predites
donnees = lecture_csv().astype(np.int)
# On creer une seconde matrice pour pouvoir analyser quels sont les items en commun des utilisateurs x et y
donneesDeBase = lecture_csv().astype(np.int)

# On parcourt les utilisateurs
for utilisateurCourant in range(len(donneesDeBase)):
    # On parcourt les items de l'utilisateur actuel
    for itemCourant in range(len(donneesDeBase[utilisateurCourant])):
        # Si un item est trouve sans note alors
        if donneesDeBase[utilisateurCourant][itemCourant] == -1:
            # On creer une nouvelle matrice initialisee avec des zeros et qui a pour taille le jeu de donnees voulu
            matrixItemCommun=np.zeros(shape=(NB_UTILISATEUR,NB_ITEM)).astype(np.int)
            # On y insere les items de base de l'utilisateur actuel
            matrixItemCommun[utilisateurCourant]=donneesDeBase[utilisateurCourant]
            # On creer une liste d'utilisateurs
            listeUtilisateur = []
            # On parcourt de nouveau la matrice pour connaitre les items en commun de l'utilisateur compare avec l'utilisateur courant
            for utilisateurCompare in range(len(donneesDeBase)):
                # On creer l'utilisateur compare
                utilisateur=Utilisateur(utilisateurCompare)
                # On parcours les items de celui-ci
                for itemCompare in range(len(donneesDeBase[utilisateurCompare])):
                    # Si l'utilisateur courant est different de l'utilisateur compare alors
                    if (utilisateurCourant != utilisateurCompare): 
                        # Si les deux utilisateurs ont un item en commun alors
                        if (donneesDeBase[utilisateurCompare][itemCompare] != -1) and (donneesDeBase[utilisateurCourant][itemCompare] != -1):
                            # On insere dans la matrice la note de l'item de l'utilisateur compare, qui est commune au deux utilisateurs
                            matrixItemCommun[utilisateurCompare][itemCompare]=donneesDeBase[utilisateurCompare][itemCompare]
                            # On incremente le nombre d'item en commun de l'utilisateur compare qu'il a avec l'utilisateur courant
                        else:
                            # Sinon on y insere un item non note
                            matrixItemCommun[utilisateurCompare][itemCompare]=-1
                # On ajoute l'utilisateur dans la liste d'utilisateurs
                listeUtilisateur += [utilisateur]
                #On calcule la moyenne et la variance de l'utilisateur courant
                moyenneUtilisateurCourant = moyenne(utilisateurCourant)
                varx = var(utilisateurCourant,moyenneUtilisateurCourant)
                # On applique le reste des calculs sur les utilisateurs compare
                if (utilisateurCourant != utilisateurCompare):
                    moyenneUtilisateurCompare = moyenne(utilisateurCompare)
                    # vary = var(utilisateurCompare,moyenneUtilisateurCompare)
                    # c = cov(utilisateurCourant,utilisateurCompare,moyenneUtilisateurCourant,moyenneUtilisateurCompare)
                    # s = similarite(utilisateurCourant,utilisateurCompare,cov(utilisateurCourant,utilisateurCompare,moyenneUtilisateurCourant,moyenneUtilisateurCompare),varx,var(utilisateurCompare,moyenneUtilisateurCompare))
                    listeUtilisateur[utilisateurCompare].setSimilarite(similarite(utilisateurCourant,utilisateurCompare,cov(utilisateurCourant,utilisateurCompare,moyenneUtilisateurCourant,moyenneUtilisateurCompare),varx,var(utilisateurCompare,moyenneUtilisateurCompare)))
            top5=sorted(listeUtilisateur, key=lambda u: u.similarite, reverse=True)[:5]
            del listeUtilisateur[:]
            donnees[utilisateurCourant][itemCourant]=moyenneItems(top5,itemCourant,donnees)
            del top5[:]

with open('donnees_collaborative_filtering.csv','wb') as csvfile:
    filewriter = csv.writer(csvfile, delimiter=' ')
    for u in range(len(donnees)):
        chaine = ''
        for i in range(len(donnees[u])):
            chaine += donnees[u][i]+' '
        filewriter.writerow([chaine])