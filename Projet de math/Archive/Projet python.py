#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 26 20:41:51 2019

@author: LY Thomas - SAINT-SAENS Hugo
"""

import csv
import numpy as np
    
# FONCTION QUI RENVOIE UN TABLEAU A DEUX DIMENSIONS
# VIA LE FICHIER CSV 
def lecture_csv():
    utilisateur = 100
    item = 1000
    fichier = '/home/hugo/Documents/Spyder/Projet de math/toy_incomplet.csv'
    donnees = np.zeros((utilisateur,item)) # On initialise la matrice [Utilisateurs,Items] à zéros
    utilisateurCourant = 0
    with open(fichier, 'r') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=' ', quotechar='|')
        for row in spamreader:
            for j in range(item): # On rempli la matrice avec les données du fichier csv
                donnees[utilisateurCourant,j] = row[j]
                
            utilisateurCourant+=1 # On passe a l'utilisateur suivant
    return donnees;

# Rentrez les données du fichier .csv dans une matrice
donnees = lecture_csv().astype(np.int64) # Transtypage du tableau de float en int
# Predire les notes des items non renseignes
# Choisir le premier utilisateur
for utilisateurCourant in range(len(donnees)):
    # Choisir le premier item non reseigne
    for itemCourant in range(len(donnees[utilisateurCourant])):
        #print(donnees[x][y])
        if donnees[utilisateurCourant][itemCourant]==-1:
            # Parcourir tout les utilisateurs U hormis l'utilisateur courant
            for utilisateurAComparer in range(len(donnees)):
                # Parcourir les items
                nbItemEnCommun=0
                tabPlaceItemCommunEtNote[]
                tabUtilisateurItem[utilisateurAComparer,nbItemEnCommun]
                for itemAComparer in range(len(utilisateurAComparer)):
                    # Si un item est trouve commun entre l'utilisateur courant et l'utilisateur alors
                    if donnees[utilisateurCourant][itemCourant]!=-1 and donnees[utilisateurAComparer][itemAComparer]!=-1:
                        nbItemEnCommun=nbItemEnCommun+1
                        