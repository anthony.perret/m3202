#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 18 08:26:13 2019
@author: LY Thomas - SAINT-SAENS Hugo
"""

import csv
import numpy as np
import math as math

#Fonction permettant d'insérer les  valeurs du fichier .cvs dans une matrice
def lecture_csv():
    utilisateur = 100
    item = 1000
    fichier = './toy_incomplet.csv'
    donnees = np.zeros((utilisateur,item)) #initialise la matrice [Utilisateurs,Items] à zéros
    utilisateurCourant = 0
    with open(fichier, 'r') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=' ')
        for row in spamreader:
            for j in range(item): #rempli la matrice avec les données du fichier csv
                donnees[utilisateurCourant,j] = row[j]
            utilisateurCourant+=1 #permet de passer à l'utilisateur suivant
    return donnees;

donnees = lecture_csv().astype(np.int64) #Transtypage du tableau de float en int
#print(type(donnees[0][50])) #on regarde que le tableau ait bien été transtypé

#calcul de chaque moyennes des utilisateurs
def moyenne(x):
    somme = 0.0
    Nbitems = 0
    for j in range(len(donnees[i])):
        if donnees[i][j] != -1:
            somme = donnees[i,j] + somme
            Nbitems += 1
    return (somme / Nbitems)

#calcul de la covariance entre l'utilisateur x et y
def covariance(x,y):
    cov = 0
    m = moyenne(x)
    m2 = moyenne(y)
    for j in range(len(donnees[i])):
        if donnees[i,j] != -1:
            cov = np.sum(donnees[i,j] - m) + np.sum(donnees[i,j] - m2)
    return cov

#calcul de la variance entre l'utilisateur x et y
def variance(x,y):
    m = moyenne(x)
    m2 = moyenne(y)
    v = 0
    for j in range(len(donnees[i])):
        v += math.sqrt(np.sum((donnees[i,j]-m)**2)) * math.sqrt(np.sum((donnees[i,j]-m2)**2))
    return (v/len(donnees[i]))

#calcul de la similarité entre l'utilisateur x et y
def similarite(x,y):
    numerateur = covariance(x,y)
    denominateur = variance(x,y)
    s = 0
    for j in range(len(donnees[i])):
        s = (numerateur/denominateur)
    return (s)

#classe definisant un utilisateur
class Utilisateur:
    def __init__(self,numeroUtilisateur):
        self.numeroUtilisateur=numeroUtilisateur
        self.nbItemEnCommun=0
        self.tabPlaceItemCommunEtNote=np.zeros((2,1000))
    
    def incrementerNbItemEnCommun(self):
        self.nbItemEnCommun = self.nbItemEnCommun + 1
        
    def getNbItemEnCommun(self):
        return self.nbItemEnCommun

donnees = lecture_csv()

NBR_SIMILARITE_UTILISATEUR = 5

for utilisateurCourant in range(len(donnees)):
    for itemCourant in range(len(donnees[utilisateurCourant])):
        if donnees[utilisateurCourant][itemCourant] == -1:
            # On creer un tableau qui contiendra des utilisateurs comparé
            tabUtilisateurItem = []
            for utilisateurCompare in range(len(donnees)):
                for itemCompare in range(len(donnees[utilisateurCompare])):
                    # On verifie que l'on compare pas le meme utilisateur et le meme item
                    if (utilisateurCourant != utilisateurCompare) and (itemCourant != itemCompare):
                        # On initialise un nouveau utilisateur en fonction de sa position dans le tableau
                        u = Utilisateur(utilisateurCompare)
                        # On regarde si les items sont en communs aux deux utilisateurs
                        if (donnees[utilisateurCourant][itemCourant] != -1) and (donnees[utilisateurCompare][itemCompare] != -1):
                            # On incremente le nombre d'item en commun
                            u.incrementerNbItemEnCommun
                            # On ajoute la place de l'item et sa note dans tabPlaceItemCommunEtNote dans Utilisateur
                            u.tabPlaceItemCommunEtNote.append(itemCompare,donnees[utilisateurCompare][itemCompare])
                # On ajoute l'utilisateur dans le tableau
                tabUtilisateurItem.append(u)
                # On initialise u a null pour reutiliser la variable
                u = None
            # On parcours tabUtilisateurItem pour trouver les 5 utilisateurs avec le plus grand nombre d'item en commun
            sorted(tabUtilisateurItem, key=lambda Utilisateur: Utilisateur.getNbItemEnCommun)
            tabTop5UtilisateurItem = []
            for NBR in range(0,4):
                tabTop5UtilisateurItem[NBR] = tabUtilisateurItem[NBR]
            
    #calcul la moyenne des notes de chaque utilisateur
    #for j in range(len(donnees[i])):
    #cov = covariance(xi,xj)
    #var = variance(xi,xj)
    #s = similarite(xi,xj)
    #index += 1
    #print(index, round(cov,2), round(var,2), round(s,1))