#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 18 08:26:13 2019
@author: LY Thomas - SAINT-SAENS Hugo
"""

import csv
import numpy as np
import math as math

#Fonction permettant d'insérer les  valeurs du fichier .cvs dans une matrice
def lecture_csv():
    utilisateur = 100
    item = 1000
    fichier = './toy_incomplet.csv'
    donnees = np.zeros((utilisateur,item)) #initialise la matrice [Utilisateurs,Items] à zéros
    utilisateurCourant = 0
    with open(fichier, 'r') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=' ')
        for row in spamreader:
            for j in range(item): #rempli la matrice avec les données du fichier csv
                donnees[utilisateurCourant,j] = row[j]
            utilisateurCourant+=1 #permet de passer à l'utilisateur suivant
    return donnees;

donnees = lecture_csv().astype(np.int64) #Transtypage du tableau de float en int
#print(type(donnees[0][50])) #on regarde que le tableau ait bien été transtypé

#calcul de chaque moyennes des utilisateurs
def moyenne(k):
    somme = 0.0
    for j in range(len(donnees[k])):
        if (donnees[k,j] != -1):
            somme = donnees[k,j] + somme
    return (somme / len(donnees[i]))

#calcul de la covariance entre l'utilisateur x et y
def cov(x,y):
    cov = 0
    m1 = moyenne(x)
    m2 = moyenne(y)
    for j in range(len(donnees[i])):
            cov = cov + ((donnees[x,j] - m1)*(donnees[y,j] - m2))
    return (cov/len(donnees[i]))

#calcul de la variance entre l'utilisateur x et y
def var(k):
    m = moyenne(k)
    v = 0
    for j in range(len(donnees[i])):
        if (donnees[k,j] != -1):
            v = v + (donnees[k,j]-m)**2
    return (v/len(donnees[i]))

#calcul de la similarité entre l'utilisateur x et y
def similarite(x,y):
    num = cov(x,y)
    deno = math.sqrt(var(x))*math.sqrt(var(y))
    for j in range(len(donnees[i])):
        if (donnees[x,j] != -1 and donnees[y,j] != -1):
            return (num / deno)

subm = []
for i in range(0,10):
    aux = []
    for j in range(0,100):
        aux.append(donnees[i][j])
        
    subm.append(aux)
"""
#Parcours de la matrice
index = 0
x = 40
y = 0
liste = []
for i in range(len(donnees)):
    mx = moyenne(x)
    my = moyenne(y)
    c = cov(x,y)
    varx = var(x)
    vary = var(y)
    s = similarite(x,y)
    index += 1
    y += 1
    liste += [s]
    print(index, round(mx,2), round(my,2), round(c,2), round(vary,2),round(s,3))

top_five = sorted(liste, reverse=True)[:5] 
print(top_five)
"""