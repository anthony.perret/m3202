#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 18 08:26:13 2019

@author: LY Thomas - SAINT-SAENS Hugo
"""

import csv
import numpy as np

#Fonction permettant d'insérer les  valeurs du fichier .cvs dans une matrice
def lecture_csv():
    utilisateur = 100
    item = 1000
    fichier = './toy_incomplet.csv'
    donnees = np.zeros((utilisateur,item)) #initialise la matrice [Utilisateurs,Items] à zéros
    utilisateurCourant = 0
    with open(fichier, 'r') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=' ')
        for row in spamreader:
            for j in range(item): #rempli la matrice avec les données du fichier csv
                donnees[utilisateurCourant,j] = row[j]
            utilisateurCourant+=1 #permet de passer à l'utilisateur suivant
    return donnees;

donnees = lecture_csv().astype(np.int64) #Transtypage du tableau de float en int
print(type(donnees[0][50])) #on regarde que le tableau ait bien été transtypé

#Parcours de la matrice
for x in range(len(donnees)):
    for y in range(len(donnees[x])):
        #print(donnees[x][y])
        if donnees[x][y]==-1:
            