#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 18 08:26:13 2019
@author: LY Thomas - SAINT-SAENS Hugo
"""

import csv
import numpy as np
import math as math

#Fonction permettant d'insérer les  valeurs du fichier .cvs dans une matrice
def lecture_csv():
    utilisateur = 5
    item = 10
    fichier = './5par10.csv'
    donnees = np.zeros((utilisateur,item)) #initialise la matrice [Utilisateurs,Items] à zéros
    utilisateurCourant = 0
    with open(fichier, 'r') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=' ')
        for row in spamreader:
            for j in range(item): #rempli la matrice avec les données du fichier csv
                donnees[utilisateurCourant,j] = row[j]
            utilisateurCourant+=1 #permet de passer à l'utilisateur suivant
    return donnees;

"""
#calcul de chaque moyennes des utilisateurs
def moyenne(utilisateur,matrice):
    somme = 0.0
    Nbitems = 0
    for i in range(len(matrice[utilisateur])):
        if matrice[utilisateur][i] != -1:
            somme = matrice[utilisateur,i] + somme
            Nbitems += 1
    return (somme / Nbitems)

#calcul de la covariance entre l'utilisateur x et y
def numerateur(x,y,moyenneX,moyenneY,matrice):
    cov = 0.0
    for j in range(len(matrice[utilisateurCommun])):
        if donnees[utilisateurCommun,j] != -1:
            cov = np.sum((matrice[x,j] - moyenneX) + (matrice[y,j] - moyenneY))
    return cov

#calcul de la variance entre l'utilisateur x et y
def denominateur(x,y,moyenneX,moyenneY,matrice):
    v = 0
    for j in range(len(matrice[utilisateurCommun])):
        v += ( math.sqrt(np.sum((matrice[x,j]-moyenneX)**2)) ) * ( math.sqrt(np.sum((matrice[y,j]-moyenneY)**2)) )
    return (v/len(matrice[utilisateurCommun]))

#calcul de la similarité entre l'utilisateur x et y
def similarite(x,y,num,deno,matrice):
    for j in range(len(matrice[utilisateurCommun])):
        return (num / deno)
"""

#calcul de chaque moyennes des utilisateurs
def moyenne(k):
    somme = 0.0
    Nbitems = 0
    for j in range(len(matrixItemCommun[k])):
        if matrixItemCommun[k][j] != -1:
            somme = matrixItemCommun[k,j] + somme
            Nbitems += 1
    return (somme / Nbitems)

#calcul de la covariance entre l'utilisateur x et y
def numerateur(x,y):
    cov = 0
    m = moyenne(x)
    m2 = moyenne(y)
    for j in range(len(matrixItemCommun[utilisateurCommun])):
        if matrixItemCommun[utilisateurCommun,j] != -1:
            cov = np.sum((matrixItemCommun[x,j] - m) + (matrixItemCommun[y,j] - m2))
    return cov

#calcul de la variance entre l'utilisateur x et y
def denominateur(x,y):
    m = moyenne(x)
    m2 = moyenne(y)
    v = 0
    for j in range(len(matrixItemCommun[utilisateurCommun])):
        v += math.sqrt(np.sum((matrixItemCommun[x,j]-m)**2)) * math.sqrt(np.sum((matrixItemCommun[y,j]-m2)**2))
    return (v/len(matrixItemCommun[utilisateurCommun]))

#calcul de la similarité entre l'utilisateur x et y
def similarite(x,y):
    num = numerateur(x,y)
    deno = denominateur(x,y)
    for j in range(len(matrixItemCommun[utilisateurCommun])):
        return (num / deno)

class Utilisateur:
    def __init__(self,numero):
        self.numero=numero
        # self.nbItemCommun=0
        self.similarite=0.0
    """
    def incrementerNbItemCommun(self):
        self.nbItemCommun += 1
    """
    def setSimilarite(self,s):
        self.similarite=s
    
    def getNumero(self):
        return self.numero

def moyenneItems(top5liste,itemCourant,matrice):
    somme = 0
    nbElement = 0
    for i in range(len(top5liste)):
        if matrice[top5liste[i].getNumero()][itemCourant] != -1:
            somme += matrice[top5liste[i].getNumero()][itemCourant]
            nbElement += 1
    return somme / nbElement

donnees = lecture_csv().astype(np.int)

for utilisateurCourant in range(len(donnees)):
    for itemCourant in range(len(donnees[utilisateurCourant])):
        if donnees[utilisateurCourant][itemCourant] == -1:
            matrixItemCommun=np.zeros(shape=(5,10)).astype(np.int)
            matrixItemCommun[utilisateurCourant]=donnees[utilisateurCourant]
            x=utilisateurCourant
            listeUtilisateur = []
            for utilisateurCompare in range(len(donnees)):
                utilisateur=Utilisateur(utilisateurCompare)
                for itemCompare in range(len(donnees[utilisateurCompare])):
                    if (utilisateurCourant != utilisateurCompare): 
                        if (donnees[utilisateurCompare][itemCompare] != -1) and (donnees[x][itemCompare] != -1):
                            matrixItemCommun[utilisateurCompare][itemCompare]=donnees[utilisateurCompare][itemCompare]
                            # utilisateur.incrementerNbItemCommun
                        else:
                            matrixItemCommun[utilisateurCompare][itemCompare]=-1
                listeUtilisateur += [utilisateur]
            # moyenneUtilisateurCourant = moyenne(utilisateurCourant,matrixItemCommun)
            mx = moyenne(x)
            for utilisateurCommun in range(len(matrixItemCommun)):
                for itemCommun in range(len(matrixItemCommun[utilisateurCommun])):
                    if (utilisateurCourant != utilisateurCommun):
                        """
                        moyenneUtilisateurCommun = moyenne(utilisateurCommun,matrixItemCommun)
                        numerateur = numerateur(utilisateurCourant,utilisateurCommun,moyenneUtilisateurCourant,moyenneUtilisateurCommun,matrixItemCommun)
                        denominateur = denominateur(utilisateurCourant,utilisateurCommun,moyenneUtilisateurCourant,moyenneUtilisateurCommun,matrixItemCommun)
                        similarite = similarite(utilisateurCourant,utilisateurCommun,numerateur,denominateur,matrixItemCommun)
                        """
                        my = moyenne(utilisateurCommun)
                        n = numerateur(x,utilisateurCommun)
                        d = denominateur(x,utilisateurCommun)
                        s = similarite(x,utilisateurCommun)
                        listeUtilisateur[utilisateurCommun].setSimilarite(s)
            top5=sorted(listeUtilisateur, key=lambda u: u.similarite, reverse=True)[:5]
            del listeUtilisateur[:]
            donnees[utilisateurCourant][itemCourant]=moyenneItems(top5,itemCourant,donnees)
            del top5[:]